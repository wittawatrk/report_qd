'use strict';

const express =  require('express');
const bodyParser = require('body-parser');
const session =  require('express-session');


module.exports = () => {
    const app = express();
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(require('./router')());
    
    return app;
}