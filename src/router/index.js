const express = require('express');
const router = express.Router();
const path = require('path');
module.exports = () => {
    router.use((req,res,next)=>{
        res.setHeader('Access-Control-Allow-Origin','*');
        next();
    })
  //  router.use('/public', express.static(path.join(__dirname, '..', '..', 'public')))
    router.get('/report/:from/:to', require('../controller/getData'));
  
    return router;
}