module.exports = [{
    id:1,
    name: 'Iphone X',
    price: 990,
    img_url: 'iphone-v001.png',
    tag: [
        'mobile',
        'gadget', 
        'electronic'
    ]
},{
    id:2,
    name: 'Iphone XX',
    price: 9900,
    img_url: 'iphone-v001.png',
    tag: [
        'mobile',
        'gadget', 
        'electronic'
    ]
},
{
    id:3,
    name: 'Iphone XXX',
    price: 99000,
    img_url: 'iphone-v001.png',
    tag: [
        'mobile',
        'gadget', 
        'electronic'
    ]
},
{
    id:4,
    name: 'Iphone XXXXX',
    price: 9900000,
    img_url: 'iphone-v001.png',
    tag: [
        'mobile',
        'gadget', 
        'electronic'
    ]
}]