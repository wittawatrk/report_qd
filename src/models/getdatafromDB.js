 function getdata(req,data) {
     const conn = require('./config').connection;
     const config =require('./config').config;
    console.log(req);
     var sql = `SELECT 
     "orsrg"."ordernr" as PoNo, 
     "gbkmut"."bkstnr" as EntryNo,
     "orkrg"."orddat" as PoDate, 
     "orsrg"."PlannedDate", 
     "orsrg"."artcode" as ItemCode,
     "orsrg"."oms45" as ItemDes,
     "gbkmut"."afldat" as ReceiptDate,
     "cicmpy"."crdcode" as SupCode, 
     "Credit"."naam" as SupName, 
     Datediff(day,gbkmut.afldat,orsrg.PlannedDate) as CalDate,
     "gbkmut"."reknr", 
     "gbkmut"."aantal" as QTY, 
     "cicmpy"."TextField7" as AVL
   FROM 
     (
       (
         (
           (
             "001"."dbo"."gbkmut" "gbkmut" 
             INNER JOIN "001"."dbo"."ItemAccounts" "ItemAccounts" ON (
               "gbkmut"."artcode" = "ItemAccounts"."ItemCode"
             ) 
             AND (
               "gbkmut"."crdnr" = "ItemAccounts"."crdnr"
             )
           ) 
           INNER JOIN "001"."dbo"."orsrg" "orsrg" ON (
             (
               "gbkmut"."bkstnr_sub" = "orsrg"."ordernr"
             ) 
             AND (
               "gbkmut"."afldat" = "orsrg"."afldat"
             )
           ) 
           AND (
             "gbkmut"."artcode" = "orsrg"."artcode"
           )
         ) 
         INNER JOIN "001"."dbo"."Credit" "Credit" ON "ItemAccounts"."crdnr" = "Credit"."crdnr"
       ) 
       INNER JOIN "001"."dbo"."cicmpy" "cicmpy" ON "Credit"."crdnr" = "cicmpy"."crdnr"
     ) 
     INNER JOIN "001"."dbo"."orkrg" "orkrg" ON "orsrg"."ordernr" = "orkrg"."ordernr" 
   WHERE 
     "gbkmut"."bkstnr" <> '' 
     AND (
       "gbkmut"."reknr" LIKE '   117300' 
       OR "gbkmut"."reknr" LIKE '   117400' 
       OR "gbkmut"."reknr" LIKE '   117600' 
       OR "gbkmut"."reknr" LIKE '   117700'
     ) 
     AND NOT (
       "orsrg"."artcode" LIKE '---%' 
       OR "orsrg"."artcode" LIKE '00-%' 
       OR "orsrg"."artcode" LIKE 'EXP%' 
       OR "orsrg"."artcode" LIKE 'FA%' 
       OR "orsrg"."artcode" LIKE 'LOGI%' 
       OR "orsrg"."artcode" LIKE 'NO CODE%' 
       OR "orsrg"."artcode" LIKE 'NOCODE%'
     ) 
     AND (
       "gbkmut"."afldat" >= '${req.from}'  
       AND "gbkmut"."afldat" <= '${req.to}' 
     ) 
   ORDER BY 
     "gbkmut"."crdnr", 
     "gbkmut"."bkstnr"`;


     var dbcon = conn.connect(config, (err) => {
         if (err) {
             console.log(err)
         } else {
             console.log('connected')
             request = new conn.Request(dbcon);

         }
         request.query(sql, function (err, recordset) {

             if (err) console.log(err);

             data(null, recordset);
             conn.close((err) => {
                 if (err) console.log(err);
                 else console.log('connection closed');
                 return;
             })
         });

     });

 }
 module.exports = {getdata};