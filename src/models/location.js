const location = require('./locations.json');

module.exports={
    getProvince,
    getDistrictByProvincecode
};


function getProvince(callback) {
    let province = [];
    location.forEach(item => {
        const index = province.findIndex((pro) => pro.code === item.province_code);

        if (index > -1) {

        } else {
            let data = {};
            data.code = item.province_code;
            data.name = item.province;
            province.push(data);

        }

    });
    province.sort(function (a, b) {
        return a.name.localeCompare(b.name);
    });
    callback(null, province);

}

function getDistrictByProvincecode(code, callback) {
    let amphoe = [];
    const provinceByid = location.filter((proid) => proid.province_code == code)
    
    provinceByid.forEach((item) => {
        const index = amphoe.findIndex((pro) => pro.amphoe_code === item.amphoe_code);
        if (index > -1) {

        } else {
            let data = {};
            data.amphoe_code = item.amphoe_code;
            data.amphoe_name = item.amphoe;
            data.zipcode = item.zipcode;

            amphoe.push(data);

        }
        amphoe.sort(function (a, b) {
            return a.amphoe_name.localeCompare(b.amphoe_name);
        });
    })



    callback(null, amphoe)

}

